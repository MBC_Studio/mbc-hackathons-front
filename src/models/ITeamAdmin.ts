import {IUser} from "./IUser";
import {IHackathon} from "./IHackathon";

export interface ITeamAdmin {
    id: string;
    title: string;
    description?: string;
    hackathon: IHackathon;
    status: string;
    captain: IUser;
    answer: string;
    score: number;
    members: IUser[];
}

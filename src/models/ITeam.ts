import {IUser} from "./IUser";


export interface ShortHackathon {
    id: string;
    title: string;
    maxTeamSize: number;
    endApplicationsDate: number;
    startDate: number;
    endDate: number;
}

export interface ITeam {
    id: string;
    title: string;
    description?: string;
    hackathon: ShortHackathon;
    status: string;
    captain: IUser;
    members: IUser[];
}

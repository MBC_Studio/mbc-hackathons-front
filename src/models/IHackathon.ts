export interface Event {
    id: string;
    title: string;
    startTime: [number, number]; // Массив [часы, минуты]
    endTime: [number, number]; // Массив [часы, минуты]
}

export interface Step {
    id: string;
    date: [number, number, number]; // Массив [год, месяц, день]
    events: Event[];
}

export interface IHackathon {
    id: string;
    title: string;
    description: string;
    shortTask: string;
    fullTask: string;
    maxTeamSize: number;
    maxScore: number;
    steps: Step[];
    startApplicationsDate: number; // Timestamp в миллисекундах
    endApplicationsDate: number; // Timestamp в миллисекундах
    startDate: number; // Timestamp в миллисекундах
    endSolvingDate: number; // Timestamp в миллисекундах
    endDate: number; // Timestamp в миллисекундах
}

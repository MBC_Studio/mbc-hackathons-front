import React, {useContext} from 'react';
import {observer} from "mobx-react";
import {AppContext} from "../../index";
import {Navigate, Outlet} from "react-router-dom";

const IsAuth = ({children}: any) => {
    const {store} = useContext(AppContext);
    if (!store.isAuth) {
        return <Navigate to={'/login'} replace/>;
    }
    return children ? children : <Outlet/>;
};

export default observer(IsAuth);
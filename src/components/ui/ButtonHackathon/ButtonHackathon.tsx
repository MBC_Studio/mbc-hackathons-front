import React from 'react';
import cl from './ButtonHackathon.module.css';
import {Link} from "react-router-dom";

interface ButtonHackathonProps {
    buttonText: string;
    link: string;
}

const ButtonHackathon: React.FC<ButtonHackathonProps> = ({ buttonText, link }) => {
    return (
        <Link to={link} className={cl.btn}>
            {buttonText}
        </Link>
    );
};

export default ButtonHackathon;

import React from 'react';
import cl from './PrimaryButtonFull.module.css';
import {Link} from "react-router-dom";

interface PrimaryButtonProps {
    buttonText: string;
    link: string; // Добавляем пропс для URL
}

const PrimaryButtonFull: React.FC<PrimaryButtonProps> = ({ buttonText, link }) => {
    return (
        <Link to={link} className={cl.btn}>
            {buttonText}
        </Link>
    );
};

export default PrimaryButtonFull;

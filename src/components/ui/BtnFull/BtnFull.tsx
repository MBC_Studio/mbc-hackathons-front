import React from 'react';
import cl from './BtnFull.module.css';
import {Link} from "react-router-dom";

interface ButtonHackathonProps {
    text: string;
    link: string;
    backColor: string;
}

const BtnFull: React.FC<ButtonHackathonProps> = ({ text, link, backColor }) => {
    const btnStyle = {
        backgroundColor: backColor,
    };

    return (
        <Link to={link} className={cl.btn} style={btnStyle}>
            {text}
        </Link>
    );
};

export default BtnFull;

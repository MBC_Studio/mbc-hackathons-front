import React from 'react';
import cl from './Btn.module.css';
import {Link} from "react-router-dom";

interface ButtonHackathonProps {
    buttonText: string;
    link: string;
    backColor: string;
}

const Btn: React.FC<ButtonHackathonProps> = ({ buttonText, link, backColor }) => {
    const btnStyle = {
        backgroundColor: backColor,
    };

    return (
        <Link to={link} className={cl.btn} style={btnStyle}>
            {buttonText}
        </Link>
    );
};

export default Btn;
